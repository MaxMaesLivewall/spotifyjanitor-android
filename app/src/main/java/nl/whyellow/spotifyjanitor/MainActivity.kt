package nl.whyellow.spotifyjanitor

import android.os.Bundle
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import nl.whyellow.spotifyjanitor.api.LibraryAPI
import nl.whyellow.spotifyjanitor.api.RetrofitCreator
import nl.whyellow.spotifyjanitor.api.models.PagedResponse
import nl.whyellow.spotifyjanitor.api.models.SavedTrack
import nl.whyellow.spotifyjanitor.api.models.User
import org.jetbrains.anko.doAsync

const val REQUEST_CODE = 1337
const val REDIRECT_URI = "somecustomprotocol://callback"
const val CLIENT_ID = "d0a1cd3e2f4c468db0d0ab35af7ee27b" //TODO: UPDATE THIS

class MainActivity : AppCompatActivity() {
    private val libraryAPI: LibraryAPI by lazy { RetrofitCreator.createLibraryAPI() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startSpotifyLogin()
    }

    private fun startSpotifyLogin() {
        // Request code will be used to verify if result comes from the login activity. Can be set to any integer.
        val builder = AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)

        builder.setScopes(arrayOf("user-library-read", "user-library-modify", "user-read-private"))
        val request = builder.build()

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            val response = AuthenticationClient.getResponse(resultCode, intent)

            when (response.type) {
            // Response was successful and contains auth token
                AuthenticationResponse.Type.TOKEN -> {
                    // Request saved tracks from the libraryAPI and pass the access token
                    doAsync {
                        val apiResponse = libraryAPI.getMySavedTracks("Bearer ${response.accessToken}").execute()
                        val meResponse = libraryAPI.getMe("Bearer ${response.accessToken}").execute()

                        runOnUiThread {
                            processSavedTracks(apiResponse.body())
                            processMe(meResponse.body())
                        }
                    }
                }

            // Auth flow returned an error
                AuthenticationResponse.Type.ERROR -> TODO()
                AuthenticationResponse.Type.CODE -> TODO()
                AuthenticationResponse.Type.EMPTY -> TODO()
                AuthenticationResponse.Type.UNKNOWN -> TODO()
                null -> TODO()
            }// Handle successful response
            // Handle error response
            // Most likely auth flow was cancelled
            // Handle other cases
        }
    }

    private fun processMe(me: User?) {
        user_name.text = me?.displayName
        Glide.with(this)
                .load(me?.images!![0].url)
                .into(user_picture)
    }

    private fun processSavedTracks(body: PagedResponse<SavedTrack>?) {
        // TODO: Show the data here!

    }
}
