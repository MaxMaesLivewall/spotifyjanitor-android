package nl.whyellow.spotifyjanitor.api.models

data class PagedResponse<T>(
        val href: String,
        val items: List<T>,
        val next: String,
        val previous: String,
        val limit: Int,
        val offset: Int,
        val total: Int
)