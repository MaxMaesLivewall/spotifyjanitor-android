package nl.whyellow.spotifyjanitor.api

import com.squareup.moshi.Moshi
import nl.whyellow.spotifyjanitor.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitCreator {
    fun createLibraryAPI(): LibraryAPI {
        return create().create(LibraryAPI::class.java)
    }

    fun create(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.spotify.com/v1/")
                .addConverterFactory(MoshiConverterFactory.create(provideMoshi()))
                .client(provideOkHttpClient())
                .build()
    }

    private fun provideMoshi(): Moshi {
        return Moshi.Builder()
                .build()
    }

    private fun provideOkHttpClient(): OkHttpClient {
        return if(!BuildConfig.DEBUG) {
            OkHttpClient.Builder()
                    .build()
        } else {
            OkHttpClient.Builder()
                    .addNetworkInterceptor(provideHttpLogginInterceptor())
                    .build()
        }
    }

    private fun provideHttpLogginInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}