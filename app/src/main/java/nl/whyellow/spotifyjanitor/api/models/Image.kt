package nl.whyellow.spotifyjanitor.api.models

data class Image(
        val url: String?,
        val width: Int?,
        val height: Int?
)