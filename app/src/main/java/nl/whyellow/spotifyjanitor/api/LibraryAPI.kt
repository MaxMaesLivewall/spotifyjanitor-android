package nl.whyellow.spotifyjanitor.api

import nl.whyellow.spotifyjanitor.api.models.PagedResponse
import nl.whyellow.spotifyjanitor.api.models.SavedTrack
import nl.whyellow.spotifyjanitor.api.models.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface LibraryAPI {
    /**
     * Retrieves the first page of saved tracks from the user associated with the authorization header
     *
     * @param authorizationHeader The header to pass, with format: "Bearer <accesscode>"
     */
    @GET("me/tracks")
    fun getMySavedTracks(@Header("Authorization") authorizationHeader: String): Call<PagedResponse<SavedTrack>>

    /**
     * Retrieves the first page of saved tracks from the user associated with the authorization header
     *
     * @param authorizationHeader The header to pass, with format: "Bearer <accesscode>"
     * @param offset The amount of tracks to skip in the result
     * @param limit The maximum amount of tracks to retrieve
     */
    @GET("me/tracks")
    fun getMySavedTracks(
            @Header("Authorization") authorizationHeader: String,
            @Query("offset") offset: Int,
            @Query("limit") limit: Int ): Call<PagedResponse<SavedTrack>>

    @GET("me")
    fun getMe(@Header("Authorization") authorizationHeader: String): Call<User>
}