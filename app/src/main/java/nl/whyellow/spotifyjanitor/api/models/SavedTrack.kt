package nl.whyellow.spotifyjanitor.api.models

import com.squareup.moshi.Json

data class SavedTrack(
        val track: Track,
        @Json(name = "added_at") val addedAt: String
)