package nl.whyellow.spotifyjanitor.api.models

import com.squareup.moshi.Json

data class Track(
    val album: Album,
    val artists: List<Artist>,
    @Json(name = "available_markets") val availableMarkets: List<String>,
    @Json(name = "disc_number") val discNumber: Int,
    @Json(name = "duration_ms") val duration: Long,
    val explicit: Boolean,
    @Json(name = "external_ids") val externalIds: Map<String, String>,
    @Json(name = "external_urls") val externalUrls: Map<String, String>,
    val href: String,
    val id: String,
    val name: String,
    val popularity: Int,
    @Json(name = "preview_url") val previewUrl: String,
    @Json(name = "track_number") val trackNumber: Int,
    val type: String,
    val uri: String

)