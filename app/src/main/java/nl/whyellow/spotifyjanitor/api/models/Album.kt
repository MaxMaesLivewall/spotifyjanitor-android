package nl.whyellow.spotifyjanitor.api.models

import com.squareup.moshi.Json

data class Album(
        val artists: List<Artist>,
        val href: String,
        val id: String,
        val images: List<Image>,
        val name: String,
        val type: String,
        val uri: String,
        @Json(name = "album_type") val albumType: String,
        @Json(name = "available_markets") val markets: List<String>,
        @Json(name = "external_urls") val externalUrls: Map<String, String>
)