package nl.whyellow.spotifyjanitor.api.models

import com.squareup.moshi.Json

data class Artist(
        @Json(name = "external_urls") val externalUrls: Map<String, String>,
        val href: String,
        val id: String,
        val name: String,
        val type: String,
        val uri: String
)