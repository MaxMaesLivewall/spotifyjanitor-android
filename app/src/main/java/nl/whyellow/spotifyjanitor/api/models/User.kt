package nl.whyellow.spotifyjanitor.api.models

import com.squareup.moshi.Json

data class User(
        val birthdate: String,
        val country: String,
        @Json(name = "display_name") val displayName: String,
        val images: List<Image>
)